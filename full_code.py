#!/usr/bin/env python3.4

import time
from gpiozero import Button
from classifier import classify_image
import camera
import motor
import pigpio
from stepper import *

print("The importing has finished...")

PIN = 18
CLOSE = 90 
OPEN = 0
SENSOR_PINS = {"compost": 12, "recycle": 16, "trash": 26}

# Function to swing the motor until it hits a sensor.
def turn_motor_until_sensor(m, s):
    motor.turn()
    s.wait_for_press(timeout = 5)
    motor.stop()

# Initialize sensors.
sensors = {
    "compost": Button(SENSOR_PINS["compost"]),
    "recycle": Button(SENSOR_PINS["recycle"]),
    "trash": Button(SENSOR_PINS["trash"]),
}

# Initialize motors.
motor = motor.Motor(motor.GERTBOT)
pi = pigpio.pi()
#stepper = Stepper(pi, PIN1, PIN2, PIN3, PIN4)

if __name__ == "__main__":
    print("Name was main.")
    # Start sorting trash.
    try:
        while True:
             #servo.set_angle(OPEN)
             print("Waiting for object.")
             if camera.detect():
                 #servo.set_angle(CLOSE)
                 print("Object detected!")
                 time.sleep(3)
                 camera.take_picture()
                 camera.take_picture()
                 image = camera.take_picture()
                 camera.cv2.imshow("Image", image)
                 camera.cv2.waitKey(1)
                 rubbish_type = classify_image(image)
                 rubbish_type = max(rubbish_type, key=rubbish_type.get)
                 print("Rubbish type: {}".format(rubbish_type))
                 turn_motor_until_sensor(motor, sensors[rubbish_type])
                 time.sleep(5)
                 camera.train_background_subtractor()
                 #servo.set_angle(OPEN)
    except BaseException as e:
        #servo.stop()
        motor.stop()
        raise e
