#!/usr/bin/env python
import tensorflow as tf
import sys
#import cv2
import numpy as np

# change this as you see fit
if len(sys.argv) != 2:
    sys.argv = []
    sys.argv.append(None)
    sys.argv.append(r"C:\Users\chait\acsef-2017\classifier\testing_data\coke.jpg")

image_path = sys.argv[1]

#cv2_img = cv2.imread(image_path)
#ret, cv2_encode = cv2.imencode(".jpeg", cv2_img)

# Read in the image_data
#image_data = cv2_encode.tostring()

image_data = tf.gfile.FastGFile(image_path, 'rb').read()

# Loads label file, strips off carriage return
# Just a list of labels (recycle, compost, trash)
label_lines = [line.rstrip() for line
                   in tf.gfile.GFile("./retrained_labels.txt")]

# Unpersists graph from file
with tf.gfile.FastGFile("./retrained_graph.pb", 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    _ = tf.import_graph_def(graph_def, name='')

with tf.Session() as sess:
    # Feed the image_data as input to the graph and get first prediction
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

    predictions = sess.run(softmax_tensor, \
             {'DecodeJpeg/contents:0': image_data})

    # Sort to show labels of first prediction in order of confidence
    top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
    print("top_k:", top_k)
    for node_id in top_k: print(label_lines[node_id])

    for node_id in top_k:
        human_string = label_lines[node_id]
        score = predictions[0][node_id]
        print('%s (score = %.5f)' % (human_string, score))
