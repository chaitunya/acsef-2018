#!/usr/bin/env python3

LOCAL = False

import os
if LOCAL:
    import tensorflow as tf
from io import BytesIO
from cv2 import imencode
import requests
import json

CLASSIFICATION_SERVER = "192.168.43.167"

CURRENT_DIR = os.path.dirname(__file__)
RETRAINED_LABELS = os.path.join(CURRENT_DIR, "retrained_labels.txt")
RETRAINED_GRAPH = os.path.join(CURRENT_DIR, "retrained_graph.pb")

if LOCAL:
    # Just a list of labels (recycle, compost, trash)
    label_lines = [line.rstrip() for line
    in tf.gfile.GFile(RETRAINED_LABELS)]
    # Unpersists graph from file
    with tf.gfile.FastGFile(RETRAINED_GRAPH, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())
        _ = tf.import_graph_def(graph_def, name='')
    sess = tf.Session()
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')

def classify_image(img):
    if LOCAL:
        ret, cv2_encode = imencode(".jpg", img)
        image_data = cv2_encode.tostring()
        # Feed the image_data as input to the graph and get first prediction
        predictions = sess.run(softmax_tensor, {'DecodeJpeg/contents:0': image_data})
        # Sort to show labels of first prediction in order of confidence
        top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]

        return_dict = {}

        for node_id in top_k:
            human_string = label_lines[node_id]
            score = predictions[0][node_id]
            return_dict[human_string] = score
        return return_dict
    else:
        _, image = imencode(".jpg", img)
        image = image.tobytes()
        result = requests.post("http://" + CLASSIFICATION_SERVER + "/classify", data = image)
        return json.loads(result.content.decode())


if __name__ == "__main__":
    import sys
    import cv2
    import time
    imagefile = sys.argv[1]
    image = cv2.imread(imagefile)
    oldtime = time.time()
    results = list(classify_image(image).items())
    print(time.time()-oldtime)
    results.sort(key=lambda x: x[1], reverse=True)
    for result in results:
        print(result)
