import time
import cv2
import numpy as np

capture = cv2.VideoCapture(-1)

for i in range(5):
    capture.read()

background_subtractor = cv2.createBackgroundSubtractorMOG2()

def take_picture():
    ret = False
    while ret == False:
        ret, image = capture.read()
    return image

# Train the background subtractor.
pics = [take_picture() for i in range(10)]
for pic in pics:
    background_subtractor.apply(pic)

def detect():
    image = take_picture()
    mask = background_subtractor.apply(image)
    cv2.imshow("Mask", mask)
    cv2.imshow("Image", image)
    cv2.waitKey(1)
    image_width = capture.get(cv2.CAP_PROP_FRAME_WIDTH)
    image_height = capture.get(cv2.CAP_PROP_FRAME_HEIGHT)
    # If more than 1/3 of the image is white, then return true:
    if np.sum(mask) > (image_width * image_height * 255) / 3:
        return True
    return False

def train_background_subtractor():
    global background_subtractor
    background_subtractor = cv2.createBackgroundSubtractorMOG2()
    pics = []
    for i in range(15):
	    pics.append(take_picture())
	    time.sleep(0.2)
    for pic in pics:
        background_subtractor.apply(pic)
