#!/usr/bin/env python3.4

import time
import readline
import gertbot as gb

gb.open_uart(0)
gb.set_mode(0, 1, gb.MODE_BRUSH)
gb.set_brush_ramps(0, 1, gb.RAMP_050, gb.RAMP_050, 0)
gb.pwm_brushed(0, 1, 5000, 100)

try:
	while True:
		cmd = input("cc or ccw? ")
		if cmd.lower() == 'cc':
			gb.move_brushed(0, 1, gb.MOVE_B)
			time.sleep(0.3)
		elif cmd.lower() == 'ccw':
			gb.move_brushed(0, 1, gb.MOVE_A)
			time.sleep(0.3)
		elif cmd.lower() == 'q':
			gb.emergency_stop()
			exit()
		gb.move_brushed(0, 1, gb.MOVE_STOP)
except BaseException as e:
	gb.emergency_stop()
	exit()
