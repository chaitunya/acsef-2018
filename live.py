#!/usr/bin/env python3.4
import cv2

cap = cv2.VideoCapture(-1)

if not cap.isOpened():
	print("Could not open camera...")
	exit()

def takePicture():
    img = None
    while type(img) == type(None):
        _, img = cap.read()
    return img

try:
    while True:
        cv2.imshow("Webcam Image", takePicture())
        if cv2.waitKey(1) == ord(' '):
                break
except KeyboardInterrupt:
    cap.release()
