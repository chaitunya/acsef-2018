"""
Module for stepper motors
Currently just have turns, soon will be able to set positions
"""
import pigpio
from time import sleep


class Stepper:
    def __init__(self, pi, pins, steps=512):
        """

        :param pi: pigpio main pi object
        :param pins: pins for stepper motors (4 total)
        :param steps: number of turns there are in a full rotation
        """
        self.pins = pins
        self.pi = pi
        self.steps = steps
        for pin in self.pins:
            self.pi.set_mode(pin, pigpio.OUTPUT)
        self._current_pos = 0

    def turn(self, turns):
        """
        :argument turns The number of turns you want to take.
        """
        if turns > 0:
            for i in range(turns):
                # One turn
                for num, pin in enumerate(self.pins):
                    self.pi.write(pin, 1)
                    sleep(0.01)
                    self.pi.write(self.pins[num-1], 0)
                    sleep(0.01)
        # In case of negative turn
        elif turns < 0:
            for i in range(turns):
                for num, pin in enumerate(self.pins[::-1]):
                    self.pi.write(pin, 1)
                    sleep(0.01)
                    self.pi.write(self.pins[::-1][num-1], 0)
                    sleep(0.01)
        self._current_pos += turns

    @property
    def current_pos(self):
        self._current_pos %= self.steps
        return self._current_pos

    def set_pos(self, position):
        self.turn(position - self.current_pos)

    def set_zero(self):
        self._current_pos = 0

    def quit(self):
        for pin in self.pins:
            self.pi.write(pin, 0)



if __name__ == "__main__":
    stepper = Stepper(pigpio.pi(), [10, 11, 12, 13])
    stepper.turn(256)
