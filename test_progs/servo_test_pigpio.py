import pigpio
from time import sleep

pwm_max = 150
pwm_min = 513
pwm_pin = 18
pwm_diff = pwm_min-pwm_max
pi = pigpio.pi()
pi.set_PWM_frequency(18, 50)
pi.set_PWM_range(18, 4096)
print(pwm_min)
pi.set_PWM_dutycycle(pwm_pin, pwm_min)
sleep(1)
print(pwm_max)
pi.set_PWM_dutycycle(pwm_pin, pwm_max)
sleep(1)
pi.set_PWM_dutycycle(pwm_pin, 0)
print("pwm diff:", pwm_diff)
# Previous PWM diff = 490
