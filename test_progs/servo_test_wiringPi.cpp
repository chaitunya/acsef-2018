#include <iostream>
#include <wiringPi.h>

using namespace std;
int main() {
    wiringPiSetup();
    pinMode(1, PWM_OUTPUT);
    pwmWrite(1, 38);
    cout << "pwmWrite at 38/1024";
    delay(1000);
}