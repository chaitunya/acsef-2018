from Adafruit_PCA9685 import PCA9685 as PWM
from time import sleep

pwm = PWM(0x40)
pwm_max = 150
pwm_min = 600
pwm_pin = 0
pwm_diff = pwm_min-pwm_max
pwm.set_pwm_freq(60)
pwm.set_pwm(pwm_pin, 0, pwm_min)
sleep(1)
pwm.set_pwm(pwm_pin, 0, pwm_max)
sleep(1)
pwm.set_all_pwm(0, 0)
print("pwm diff:", pwm_diff)
# Previous PWM diff = 490
