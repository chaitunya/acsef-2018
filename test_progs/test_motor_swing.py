#!/usr/bin/env python3

import time
from gpiozero import Button
import motor

# Function to swing the motor until it hits a sensor.
def turn_motor_until_sensor(m, s):
    m.turn()
    s.wait_for_press()
    m.stop()

SENSOR_PINS = {"compost": 12, "recycle": 16, "trash": 26}
# Initialize sensors.
sensors = {
    "compost": Button(SENSOR_PINS["compost"]),
    "recycle": Button(SENSOR_PINS["recycle"]),
    "trash": Button(SENSOR_PINS["trash"]),
}

# Initialize motor.
motor = motor.Motor(motor.GERTBOT)

# Start sorting trash.
try:
    while True:
        # Move the motor.
        rubbish_type = input("Rubbish type: ")
        turn_motor_until_sensor(motor, sensors[rubbish_type])
except KeyboardInterrupt:
    print()
