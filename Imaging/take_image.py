import cv2
import sys
import os
cap = cv2.VideoCapture(0)
retval, image = cap.read()
if retval:
    filename = input("Which image do you want to write to (don't include extension)? ")
    try:
        cv2.imwrite(os.environ["HOME"] + "/acsef-2017/Classifier/tf_files/testing_data/" + filename + ".png", image)
    except Exception as e:
        cap.release()
        print("Oops. I had an error:\n " + str(e))
else:
    print("Oops. I can't capture image. Is the camera connected?")
cap.release()

