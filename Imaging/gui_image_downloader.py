#!/usr/bin/env python3

import subprocess
import requests
import bs4
import shutil
import random
import argparse
import time
import cv2
import os
import numpy as np

argparser = argparse.ArgumentParser(description="Download images from Google Images based on a comma-separated list of queries.")
argparser.add_argument("--evade-google", help="Use random sleeps between downloads to evade Google. Set EVADE_GOOGLE to the maximum number of seconds that the program should sleep to evade Google.")
args = argparser.parse_args()
if args.evade_google is None:
    EVADE_GOOGLE = 0
else:
    EVADE_GOOGLE = int(args.evade_google)

category = input("Category? Leave blank to not check images: ")
type_of_trash = input("Type of %s material? " % category)


def check_images(image_filename):
    dirname = os.path.join(os.path.dirname(__file__), os.path.join("../classifier/training_data/", category))
    files = os.listdir(dirname)
    img = cv2.imread(image_filename)
    for file in files:
        check_img = cv2.imread(os.path.join(dirname, file))
        if np.all(img == check_img):
            return True
    return False

SCREEN_WIDTH = int(subprocess.check_output(["tput", "cols"]))
BASE_URL = "https://www.google.com/search?tbm=isch&q={}"
IMAGE_FILE_NAME_PATTERN = "{}_{}.jpeg"


queries_to_download = input("Comma-separated list of queries to download? ").replace(", ",',').split(',')
print("Queries:")
for query in queries_to_download:
    print("\t-{}".format(query))

for query_i, query in enumerate(queries_to_download):
    print(u"\u2500" * SCREEN_WIDTH)
    print("Query: '{}'".format(query))
    print("{}% of queries completed.".format(round((query_i / len(queries_to_download)) * 100), 2))
    google_images_webpage = requests.get(BASE_URL.format(query)).text
    google_images_soup = bs4.BeautifulSoup(google_images_webpage, "html.parser")
    image_tags = google_images_soup.find_all("img")
    for image_i,image_tag in enumerate(image_tags):
        print("{}% of images for query '{}' complete.".format(round((image_i / len(image_tags)) * 100), query))
        image_url = image_tag["src"]
        image_data = requests.get(image_url, stream=True)
        filename = os.path.join(os.path.dirname(__file__), "../classifier/testing_data/", type_of_trash, IMAGE_FILE_NAME_PATTERN.format(query,image_i))
        image_file = open(filename,'wb')
        image_data.raw.decode_content = True
        shutil.copyfileobj(image_data.raw, image_file)
        image_file.close()
        if not check_images(filename) and not category == '':
            cv2.imshow(query, cv2.imread(filename))
            key = cv2.waitKey()
            cv2.destroyAllWindows()
            if key == ord('n'):
                print("Image discarded.")
                os.remove(filename)
            else:
                print("Image kept.")
            random_sleep_time = random.randint(0, EVADE_GOOGLE)
            print("Sleeping {} seconds for Google-evasion.".format(random_sleep_time))
            time.sleep(random_sleep_time)
        else:
            print("Image discarded.")
            os.remove(filename)

