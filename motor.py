import gertbot as gb
from gpiozero import Motor

GERTBOT = 0
H_BRIDGE = 1
BOARD = 0
MOTOR1 = 1

class Motor:
	def __init__(self, backend):
		self.backend = backend
		if self.backend == GERTBOT:
			gb.open_uart(0)
			gb.set_mode(BOARD, MOTOR1, gb.MODE_BRUSH)
			gb.set_brush_ramps(BOARD, MOTOR1, gb.RAMP_050, gb.RAMP_050, 0)
			gb.pwm_brushed(BOARD, MOTOR1, 5000, 60)
			self.direction = gb.MOVE_A
		elif self.backend == H_BRIDGE:
			self.motor = Motor(18, 23)
			self.direction = "forward"
	def turn(self):
		if self.backend == GERTBOT:
			gb.move_brushed(BOARD, MOTOR1, self.direction)
			if self.direction == gb.MOVE_A:
				self.direction = gb.MOVE_B
			elif self.direction == gb.MOVE_B:
				self.direction = gb.MOVE_A
		elif self.backend == H_BRIDGE:
			if self.direction == "forward":
				self.motor.forward()
				self.direction = "backward"
			elif self.direction == "backward":
				self.motor.backward()
				self.direction = "forward"
	def turn_opposite_direction(self):
		if self.backend == GERTBOT:
			gb.move_brushed(BOARD, MOTOR1, self.direction)
	def stop(self):
		if self.backend == GERTBOT:
			gb.move_brushed(BOARD, MOTOR1, gb.MOVE_STOP)
		elif self.backend == H_BRIDGE:
			self.motor.stop()
	def __del__(self):
		if self.backend == GERTBOT:
			gb.emergency_stop()
