#!/usr/bin/env python3
import cv2
import os
from classifier import classify_image

"""
image name,compost percentage,recycling percentage,trash percentage,correctness
can_0.jpeg,0,100,0,True
banana_0.jpeg,0,100,0,False
"""

csv_file = open("data.csv", 'w')
csv_file.write("image name,compost percentage,recycling percentage,trash percentage,correctness,category\n")

labels = ["recycle", "trash", "compost"]
DIR = "classifier/testing_data/"
filenames = []
for label in labels:
    filenames += [os.path.join(DIR, label, filename) for filename in os.listdir(os.path.join(DIR, label))]

number_right = 0

for filename in filenames:
    img = cv2.imread(filename)
    results_dict = classify_image(img)
    results = list(results_dict.items())
    results.sort(key=lambda x: x[1], reverse=True)
    category = os.path.basename(os.path.dirname(filename))
    if results[0][0] == category:
        correctness = True
    else:
        correctness = False
    csv_line = "{},{},{},{},{},{}\n".format(os.path.basename(filename), results_dict["compost"], results_dict["recycle"], results_dict["trash"], correctness, category)
    csv_file.write(csv_line)
    if results[0][0] == category:
        number_right += 1
    else:
        print(filename)
    if results[0][1] < 0.5:
        print(results[0])

print(str(number_right/len(filenames) * 100) + "%")
print("Total images: %i" % len(filenames))
csv_file.close()
