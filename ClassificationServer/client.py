#!/usr/bin/env python3

SERVER = "http://localhost"

import requests
import cv2

camera = cv2.VideoCapture(0)


def takePicture(cam):
    while True:
        status, image = cam.read()
        if status:
            return image


while True:
    image = takePicture(camera)
    _, image = cv2.imencode(".jpg", image)
    image = image.tobytes()
    result = requests.post(SERVER + "/classify", data = image)
    print(result.content)
    break
