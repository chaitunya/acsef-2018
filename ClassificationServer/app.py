#!/usr/bin/env python3

import numpy as np
import cv2
from flask import Flask, request
import json
from classifier import classify_image

app = Flask(__name__)


@app.route("/classify", methods=["POST"])
def classify_page():
    image = np.frombuffer(request.data, np.uint8)
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    cv2.imwrite("image.jpeg", image)
    classify_dict = classify_image(image)
    for key in classify_dict.keys():
	    classify_dict[key] = float(classify_dict[key])
    sendString = json.dumps(classify_dict)
    return sendString

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, debug=True)
