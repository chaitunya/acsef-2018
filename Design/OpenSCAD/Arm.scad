PLYWOOD_THICKNESS = 0.5;
FOOT = 12;
$fn = 200;
module Arm() {
        linear_extrude(height = PLYWOOD_THICKNESS) {
            square([1.2 * FOOT, 3]);
        }
}
module Arm_End() {
    translate([1.2 * FOOT + 5, 1.2, 0]) {
        difference() {
            linear_extrude(height = PLYWOOD_THICKNESS) {
                square([10, 10], center = true);
            }
            translate([0, 0, -0.4]) {
                linear_extrude(height = PLYWOOD_THICKNESS * 2) {
                    square([8, 8], center = true);
                }
            }
        }
        translate([0, -3, -2]) {
            rotate([-$t * 90, 0, 0]) {
                linear_extrude(height = PLYWOOD_THICKNESS) {
                    square([8, 5], center = true);
                }
            }
        }
        translate([0, 3, -2]) {
            rotate([$t * 90, 0, 0]) {
                linear_extrude(height = PLYWOOD_THICKNESS) {
                    square([8, 5], center = true);
                }
            }
        }
    }
}
module Arm_End_Without_Flappers() {
    translate([1.2 * FOOT + 5, 1.2, 0]) {
        difference() {
            linear_extrude(height = PLYWOOD_THICKNESS) {
                square([10, 10], center = true);
            }
            translate([0, 0, -0.4]) {
                linear_extrude(height = PLYWOOD_THICKNESS * 2) {
                    square([8, 8], center = true);
                }
            }
        }
    }
}

projection(cut = false) {
    union() {
        Arm();
        Arm_End_Without_Flappers();
    }
}