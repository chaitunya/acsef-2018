$fn = 200;
module Motor() {
    //Motor body.
    cylinder(1.942, 1.175 / 2, 1.175 / 2, true);
    translate([0, 0, 1.942 / 2]) {
        cylinder(0.968, 1.370 / 2, 1.370 / 2, true);
    }
    //Shaft.
    translate([0.131, 0, 0.5 * (1.942 + 0.968)]) {
        cylinder(0.75, 0.1, 0.1, true);
    }
}