PLYWOOD_THICKNESS = 0.5;
FOOT = 12;

module strip() {
    square([3.5 * FOOT, 3 + PLYWOOD_THICKNESS], center = true);
}

for (i = [0:6:20]) {
    translate([0, i]) {
        strip();
    }
}