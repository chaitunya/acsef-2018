PLYWOOD_THICKNESS = 0.5;
//Top and bottom.
difference() {
    square([36, 36], center = true);
    square([4.1, 4.1], center = true);
}
translate([0, 40]) {
    difference() {
        square([36, 36], center = true);
        square([4.1, 4.1], center = true);
    }
}
//Sides.
module side() {
    translate([20, 0]) {
        square([5, 36 + PLYWOOD_THICKNESS]);
    }
}
for (i = [0:8:24]) {
    translate([i, 0]) {
        side();
    }
}