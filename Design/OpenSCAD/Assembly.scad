use <Ring.scad>;
use <Arm.scad>;
use <Motor.scad>;
use <MotorMount.scad>;
use <BallCaster.scad>;
use <Stepper.scad>;
$fn = 200;
FOOT = 12;
PLYWOOD_THICKNESS = 0.5;
//Box.
cube([36, 36, 5], center = true);
//Pole.
translate([0, 0, 3.5 * FOOT / 2]) {
    difference() {
        cube([4, 4, 3.5 * FOOT], true);
        cube([3.5, 3.5, 4 * FOOT], true);
    }
}
//Ring.
color([1, 0, 0]) {
    translate([0, 0, (3.5 * FOOT) - 2]) {
        linear_extrude(height = PLYWOOD_THICKNESS) {
            Ring();
        }
    }
}
//Motor.
translate([0, 0, 3.5 * FOOT - 1]) {
    color([1, 0, 1]) {
        translate([0, 0, 1]) {
            linear_extrude(height = PLYWOOD_THICKNESS) {
                MotorMount();
            }
        }
    }
    Motor();
}
//Arm.
rotate([0, 0, $t * 360]) {
    translate([0, -1.5, 3.5 * FOOT + 0.7]) {
        Arm();
        Arm_End();
        //Caster wheel.
        translate([11, 0, 0]) {
            rotate([180, 0, 0]) {
                scale([0.1, 0.1, 0.1]) {
                    BallCaster();
                }
                translate([0, 0, 1]) {
                    sphere(d=2);
                }
            }
        }
        //Stepper with reel.
        translate([1.2 * FOOT - 1, 2, -0.5]) {
            rotate([0, 90, 0]) {
                Stepper();
            }
        }
        //Camera.
        color([0.5, 0.5, 0.5]) {
            translate([1.15 * FOOT, 1.5, 5]) {
                rotate([90, 0, 90]) {
                    translate([-2, 1, 0]) {
                        linear_extrude(height = 0.2) {
                            text("Camera", size = 1);
                        }
                    }
                }
                cube([2, 2, 2], center = true);
            }
        }
    }
}
//Bins.
module Bin(height, radius) {
    linear_extrude(height = height * FOOT) {
        difference() {
            circle(d = radius * FOOT);
            circle(d = (radius - 0.3) * FOOT);
        }
    }
}
translate([0, 1.3 * FOOT, 0]) {
    color([0, 1, 0]) {
        Bin(3, 2.0);
    }
}
rotate(a = 120, v = [0, 0, 1]) {
    translate([0, 1.3 * FOOT, 0]) {
        color([0.3, 0.3, 0.3]) {
            Bin(3, 2.0);
        }
    }
}
rotate(a = 240, v = [0, 0, 1]) {
    translate([0, 1.3 * FOOT, 0]) {
        color([0, 0, 1]) {
            Bin(3, 2.0);
        }
    }
}
