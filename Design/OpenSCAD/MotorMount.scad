$fn = 200;
module MotorMount() {
    difference() {
        square(5, center = true);
        circle(d = 1.4, center = true);
    }
}
MotorMount();