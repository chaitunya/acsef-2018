$fn = 200;
module Stepper() {
    linear_extrude(height = 1.25) {
        square(1.67, center = true);
    }
    translate([0, 0, 1.25]) {
        linear_extrude(height = 0.945) {
            circle(d = 0.125, center = true);
        }
    }
}