// Setup.
$fn = 200;

// Outer Ring.
module outer_ring() {
    difference() {
        circle(12);
        circle(12 - 1.5);
    }
}

// Inner Ring.
module inner_ring() {
    difference() {
        square(size=6, center=true);
        square(size=4, center=true);
    }
}

// Spokes.
module spoke() {
    translate([-1.6, 2.5, 0]) {
        square(size = [2, 9], center=false);
    }
}

// Final Assembly.
module Ring() {
    union() {
        inner_ring();
        outer_ring();
        difference() {
            for (i = [0 : 120 : 360 - 120]) {
                rotate([0, 0, i]) {
                    spoke();
                }
            }
            square(size=4, center = true);
        }
    }
}
Ring();